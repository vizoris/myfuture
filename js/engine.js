$(function() {







$('.select2').select2({
    placeholder: "Поиск по трём предметам",
    maximumSelectionLength: 2,
    language: "ru"
  });


$('.select-faculty').select2({
    placeholder: "Опубликовать для факультетов",
    maximumSelectionLength: 2,
    language: "ru"
  });



$('.university-select__university').select2({
    placeholder: "Выберите ВУЗ ",
    maximumSelectionLength: 2,
    language: "ru"
  });

$('.university-select__directions').select2({
    placeholder: "Выберите направления",
    maximumSelectionLength: 2,
    language: "ru"
  });

$('.select-test').select2({
    placeholder: "Вступительные испытания",
    maximumSelectionLength: 2,
    language: "ru"
  });



$('.z10 select2-selection').click(function(){
  $(this).parents('.z10').toggleClass('active');
  console.log('ccc')
})








const swiper = new Swiper('.choice-slider', {
  // Optional parameters
  
  loop: true,
  slidesPerView: 1,
  autoHeight: true,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },


});



// Поиск в чате


$(document).mouseup(function (e){ 
  const chatSearch = $('.chat-top__search');

  chatSearch.on('click', function() {
      $(this).addClass('active');      
  });

  if (!chatSearch.is(e.target) // если клик был не по нашему блоку
          && chatSearch.has(e.target).length === 0) { // и не по его дочерним элементам
          chatSearch.removeClass('active'); // скрываем его
      }

});


// Плейсфолдер в 
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.profile-data__about');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();


})